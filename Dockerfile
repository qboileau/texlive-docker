FROM ubuntu:focal
LABEL maintainer="quentin.boileau+oss@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -y &&  apt install -y texlive \
  texlive-binaries \
  texlive-font-utils \
  texlive-fonts-extra \
  texlive-latex-extra \ 
  texlive-pictures


WORKDIR /tmp

ENTRYPOINT ["/usr/bin/pdflatex", "-interaction=nonstopmode"]
CMD ["*.tex"]

